const frisby = require('frisby');
var baseURL = 'https://jsonplaceholder.typicode.com/';

it('should return first user (id 1) in body', function (done) {
    frisby.get(baseURL + 'posts/1')
        .expect('status', 200)
        .expect('json',
            {
                "userId": 1,
                "id": 1,
            })
        .done(done);
});


it('should create a resource with given data', function (done) {
    frisby.post(baseURL + 'posts', {
        title: 'test',
        content: 'some new content',
        id: 992
    })
        .expect('status', 201)
        .inspectJSON()
        .inspectBody()
        .expect('json',
            {
                "title": "test",
                "content": "some new content",
                "id": 992
            })
        .done(done);
});